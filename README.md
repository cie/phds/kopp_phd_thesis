# Publications

- [Efficient multi-level _hp_-finite elements in arbitrary dimensions](https://doi.org/10.1016/j.cma.2022.115575)
- [Space-time _hp_-finite elements for heat evolution in laser powder bed fusion additive manufacturing](https://doi.org/10.1007/s00366-022-01719-1)

# Conferences (only topic related)

- WCCM (01/2021, online, postponed due to Covid): [abstract](material/2021-01_WCCM2020_Abstract.pdf), [slides](material/2021-01_WCCM2020.pptx)
- USNCCM (07/2021, online): [abstract](material/2021-07_USNCCM_Abstract.txt), [slides](material/2021-07_USNCCM.pptx)
- Sim-AM (09/2021, online): [abstract](material/2021-09_Sim-Am_Abstract.pdf), [slides](material/2021-09_Sim-Am.pptx)
- GIMC-SIMAI YOUNG (09/2022, Pavia): [abstract](material/2022-09_GIMC-SIMAI_Abstract.pdf), [slides](material/2022-09_GIMC-SIMAI.pptx)
- HOFEIM (05/2023, Cyprus): [abstract](material/2023-05_HOFEIM_Abstract.pdf), [slides](material/2023-05_HOFEIM.pptx)
- Sim-Am (07/2023, Munich): [abstract](material/2023-07_Sim-Am_Abstract.pdf), [slides](material/2023-07_Sim-Am.pptx)

The content of the HOFEIM 2023 and Sim-Am 2023 conferences extends the work of the thesis and is therefore not fully covered here.

### Sim-Am 2021 videos

The setup is similar to Section "5.3 Hatched square" in the thesis with a 2cm x 2cm instead of a 1cm x 1cm hatched area.

![](material/2021-09_Sim-Am_hatched_square_layer.mp4)

![](material/2021-09_Sim-Am_hatched_square_mesh.mp4)

![](material/2021-09_Sim-Am_hatched_square_zoom.mp4)

# Latex code

### Compile 

```
pdflatex main.tex
biber main.bcf
pdflatex main.tex
pdflatex main.tex
```

### Tikz

The tikz exernalized compiled Figures are commited to the repository in tikz/ and the custom command \includeTikz sets the filename and calls \includegraphics afterwards. Generally one can define this command to directly include the tikz pdfs. There are two Figures
- "Figure 2.10: Comparison of the error convergence of the continuous and  ..."
- "Figure 5.12: Comparison between single space-time slab and 128 time steps."

that use some weird magic to create a single legend for multiple plots as separate figures and include them with \ref where I want. Directly including the pdf seems to break this.

I also created many shape function pictures separately and included them in the tikz pictures. The script to generate them and the resulting pdfs are in chapters/shapefunctions and chapters/shapefunctions/pdf.

# C++ Implementation

The code repository is here: https://gitlab.lrz.de/cie/phds/kopp_phd_code 
