\begin{tikzpicture}

\defineTensorProductShapes{4}{3}

\coordinate (GapX) at ($1.7*(GapX)$);
\coordinate (GapY) at ($2.4*(GapY)$);
         
\tikzset{LineStyle/.style={semithick, line cap=round, line join=round}}
 
% Horizontal gray lines
\foreach \i in {1, 2}
    \draw[LineStyle, gray] (${\i - 0.5}*(GapY) + \i*(V) - 1.1*(U)$) -- ++($3*(GapX) + 5.85*(U)$);
 
% Shape functions and frames
\foreach \i/\j/\d in {0/0/1, 0/2/1, 1/1/0, 2/1/0, 3/1/0}
    \draw[LineStyle] ($\i*(U) + \i*(GapX) + {\j + 1}*(V) + \j*(GapY)$) -- ++(${1 - \d}*(U) - \d*(V)$);

\tensorproductShapesGrid{$-1*(GapX)-1*(GapY)$}{0}{0}{{{},
                                                      {gray_02, gray_12, gray_22, gray_32},
                                                      {gray_01, gray_11, gray_21, gray_31},
                                                      {gray_00, gray_10, gray_20, gray_30}}}
                                              
\foreach \i/\j/\d in {0/0/0, 1/0/0, 1/0/1, 1/1/1, 1/2/1, 2/0/0, 3/0/0}
    \draw[LineStyle] (${\i + 1}*(U) + \i*(GapX) + \j*(V) + \j*(GapY)$) -- ++(${\d - 1}*(U) + \d*(V)$);

% Vertical gray lines
\foreach \i in {1, 2}
    \draw[LineStyle, gray] (${\i - 0.5}*(GapX) + \i*(U) - 1.7*(V)$) -- ++($2*(GapY) + 5.45*(V)$);
 
% Polynomials in x and y
\coordinate (OX) at ($-0.80*(V)$);
\coordinate (OY) at ($-0.45*(U)$);

\newcommand{\transform}[1]{(2*#1 - 1)}
\newcommand\shapeC[1]{1/4 * sqrt(6) * (\transform{#1}^2 - 1)}
\newcommand\shapeD[1]{1/4 * sqrt(10) * (\transform{#1}^2 - 1) * \transform{#1}}

\foreach \O/\V/\Gap in {OX/U/GapX, OY/V/GapY}
{
    \draw[LineStyle] (\O) -- ++(\V) -- ++($-1*(\V) + (W)$);
    \draw[LineStyle] ($(\O) + 2*(\V) + (\Gap)$) -- ++($-1*(\V)$) -- ++($(\V) + (W)$);
    
    \draw[LineStyle, domain=0:1, samples=50, variable=\t] 
        let \p1=(\V), \p2=(W), \p3=($(\O) + 2*(\V) + 2*(\Gap)$) in plot 
            ({\t*\x1 + \shapeC{\t}*\x2 + \x3}, 
             {\t*\y1 + \shapeC{\t}*\y2 + \y3}) -- cycle;
}

\draw[LineStyle, domain=0:1, samples=50, variable=\t] 
    let \p1=(U), \p2=(W), \p3=($(OX) + 3*(U) + 3*(GapX)$) in plot 
        ({\t*\x1 + \shapeD{\t}*\x2 + \x3}, 
         {\t*\y1 + \shapeD{\t}*\y2 + \y3}) -- cycle;

% Labels
\foreach \i in {0, 1, 2, 3}
    \node[below] at ($(OX) + {\i + 1/2}*(U) + \i*(GapX) - 0.5*(V)$){$I_\i(r_0)$};

\foreach \i in {0, 1, 2}
    \node[left]  at ($(OY) + {\i + 1/2}*(V) + \i*(GapY) - 0.2*(U)$){$I_\i(r_1)$};

% Boundary type texts
\node[align=center] at ($1/2*(V) + 4.32*(U) + 4*(GapX)$){bottom\\edge: $N\vert_{(1, 0)}$};
\node[align=center] at ($3/2*(V) + 4.28*(U) + 4*(GapX) + (GapY)$){top edge:\\$N\vert_{(1, 1)}$};
\node[align=center] at ($1/2*(U) + 3*(V) + 3*(GapY)$){left edge:\\$N\vert_{(0, 0)}$};
\node[align=center] at ($3/2*(U) + 3*(V) + 3*(GapY) + (GapX)$){right edge:\\$N\vert_{(0, 1)}$};
\node[align=center] at ($3*(U) + 3*(V) + 3*(GapY) + 5/2*(GapX)$){internal (bubble)\\shape functions};

\end{tikzpicture}
