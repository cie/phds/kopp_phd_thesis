\begin{tikzpicture}

\defineMediumShapes{3}{3}

\coordinate (VX) at ($(V|-U)$);
\coordinate (VY) at ($(V) - (VX)$);
\coordinate (Grid) at ($2.8*(VY)$);

\tikzset{LineStyle/.style={semithick, line cap=round, line join=round}}

% ====== Shape functions ====== 

% Vertical grid lines
\foreach \i in {0,...,2}
    \draw[LineStyle] ($(Grid) + \i*3*(U) + \i*4*(GapX)$) -- ++($3*(V) + 4*(GapY)$);
  
% Shape functions
\mediumShapesGrid{Grid}{0}{0}{{{},
                               {gray_02,     none, gray_22},
                               {gray_01, green_11, gray_21},
                               {gray_00, green_10, gray_20}}}
                    

\mediumShapesGrid{Grid}{1}{0}{{{},
                               {},
                               {green_01, gray_11},
                               {green_00, gray_10}}}
              
% Horizontal grid lines
\draw[LineStyle] ($(Grid)$) -- ++($2*3*(U) + 2*4*(GapX)$);
\draw[LineStyle] ($(Grid) + 3*(V) + 4*(GapY)$) -- ++($2*3*(U) + 2*4*(GapX)$);

\node[above right, yshift=0.15cm, inner xsep=0pt] at ($(Grid) + 3*(V) + 4*(GapY)$){Shape functions:\vphantom{Tp}};
      
% ====== Basis functions ======
\coordinate (DX) at ($2*(U) + 0.4*(U)$);
\coordinate (DY) at ($-1.8*(V)$);
\coordinate (Basis) at (0, 0);

\foreach [count=\I from 0] \IndexListRow in {{{gray_02, none}, {gray_22, none}},
                                             {{gray_01, none}, {gray_21, none}, {green_11, green_01}, {none, gray_11}},
                                             {{gray_00, none}, {gray_20, none}, {green_10, green_00}, {none, gray_10}}}
{
  \foreach [count=\J from 0] \IndexList in \IndexListRow
  {
    \coordinate (O) at ($(Basis) + \I*(DY) + \J*(DX) + 4*(VX)$);
    
    \draw[LineStyle] ($(O) + (V)$) -- ++($2*(U)$);
    
    \foreach [count=\j from 0] \ShapeIndex in \IndexList
    {
        %\ifthenelse{\j = 0}{}{}
        \draw[LineStyle] ($(O) + \j*(U)$) -- ++(V);
        \includeShape{$(O) - 1/2*(V) + \j*(U)$}{pfem_medium}{\ShapeIndex}
    }
    
    \draw[LineStyle] (O) -- ++($2*(U)$) -- ++(V);
  }
}
      
\node[above right, yshift=0.15cm, inner xsep=0pt] at ($(Basis) + (V) + 4*(VX)$){Basis functions:\vphantom{Tp}};

% ======= Tensor-product mask =======

\coordinate (MaskU) at ($0.45*(U)$);    
\coordinate (MaskV) at ($0.5*(V)$);

\coordinate (Mask1O) at ($(Grid) + 8.9*(U) + (V) + 2*(GapY)$);
\coordinate (Mask2O) at ($(Mask1O) + 2.5*(MaskU)$);

\foreach \Position [count=\i from 0] in {Mask1O, Mask2O}
    \draw[fill=red!30, rounded corners=2pt] ($(\Position) - {(0.3 + 0.5*\i)}*(MaskU) - 1.55*(MaskV)$) -- 
        ++($3.1*(MaskV)$) -- ++($0.6*(MaskU)$) -- ++($-3.1*(MaskV)$) -- cycle;

\draw[shorten >=4pt,shorten <=4pt,<->, thick, red] ($([yshift=8pt]Mask1O) + (MaskV) + 0.1*(MaskU)$) to[out=60,in=120] 
    node[midway, above]{\textcolor{red}{$T^{(0)}\vert_{(0, 1)} \land T^{(1)}\vert_{(0, 0)}$}} ($([yshift=8pt]Mask2O) + (MaskV) - 0.3*(MaskU)$);

\mlhpMatrix{Mask1O}{MaskU}{MaskV}{{ 1, \textcolor{red}{0}, 1 },
                                   { 1, \textcolor{black}{1}, 1 },
                                   { 1, \textcolor{black}{1}, 1 }}

\mlhpMatrix{Mask2O}{MaskU}{MaskV}{{  },
                                   { \textcolor{black}{1}, 1 },
                                   { \textcolor{black}{1}, 1 }}
                                                    
\foreach \i in {-1.7, 1.7}
    \draw[LineStyle] ($(Mask1O) - 1.5*(MaskU) + \i*(MaskV)$) -- ++($6*(MaskU)$);

\foreach \i in {-1.5, 1.5, 4.5}
    \draw[LineStyle] ($(Mask1O) + \i*(MaskU) - 1.7*(MaskV)$) -- ++($3.4*(MaskV)$);
    
\node[above right, yshift=0.15cm, inner xsep=0pt] at ($(Mask1O) - 2.3*(MaskU) + 2*(V) + 2*(GapY)$){Tensor-product masks:\vphantom{Tp}};

\end{tikzpicture}
