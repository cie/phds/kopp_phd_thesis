\documentclass{article}

\usepackage[textwidth=478pt]{geometry} % (print with \the\textwidth)
\usepackage{ifthen}
\usepackage{xcolor} 
\usepackage{tikz}

\usetikzlibrary{calc}
\usetikzlibrary{external}
\tikzexternalize[prefix=./pdf/]

\begin{document}
  
% Integrated Legendre polynomials
\newcommand\shapeA[1]{1/2 * (1 - (#1))}
\newcommand\shapeB[1]{1/2 * (1 + (#1))}
\newcommand\shapeC[1]{1/4 * sqrt(6) * ((#1)^2 - 1)}
\newcommand\shapeD[1]{1/4 * sqrt(10) * ((#1)^2 - 1) * (#1)}

\newcommand\shapeConstant[1]{1}

% #1: polynomial in x
% #2: polynomial in y
% #3: scaling
% #4: fill color
% #5: file name
% #6: segment resolution
\newcommand{\drawShape}[6]
{
    \tikzsetnextfilename{#5}
    
    \begin{tikzpicture}
    
        \def\scaling{#3*478pt}
        
        % Origin and direction vectors
        \coordinate (O) at (0, 0);
        \DefineU{\scaling};
        \DefineV{\scaling};
        \DefineW{\scaling};
        \DefineBoundingBox{\scaling};
        
        % Evaluation helpers
        \newcommand{\shapeR}[1]{#1{(2*(##1 + \jSegment/4) - 1)}}
        \newcommand{\shapeS}[1]{#2{(2*(##1 + \iSegment/4) - 1)}}
        
        \newcommand{\eval}[2]{{##1*\x1 + ##2*\x2 + \shapeR{##1}*\shapeS{##2}*\x3 + \x4}, 
                              {##1*\y1 + ##2*\y2 + \shapeR{##1}*\shapeS{##2}*\y3 + \y4}}
        
        % Loop over 16 segments
        \foreach \iSegment in {3,...,-1}
        {
            \foreach \jSegment in {0,...,4}
            {
                \coordinate (Segment) at ($(O) + \jSegment/4*(U) + \iSegment/4*(V)$);
                         
                % Draw segment interior
                \ifthenelse{\iSegment > -1 \AND \jSegment < 4} {
                    \fill[domain=0:1/4,samples=#6, variable=\t, #4] 
                        let \p1=(U), \p2=(V), \p3=(W), \p4=(Segment) in 
                            plot (\eval{\t}{0}) --
                            plot (\eval{1/4}{\t}) --
                            plot (\eval{(1/4-\t)}{1/4}) --
                            plot (\eval{0}{(1/4-\t)}); }{}
                  
                % Horizontal lines
                \ifthenelse{\jSegment < 4} {             
                    \draw[semithick, domain=0:1/4,samples=#6, variable=\t, line cap=round] 
                        let \p1=(U), \p2=(V), \p3=(W), \p4=(Segment) in 
                            plot (\eval{(1/4-\t)}{1/4}); }{}             
                      
                % Vertical lines
                \ifthenelse{\iSegment > -1} {             
                    \draw[semithick, domain=0:1/4,samples=#6, variable=\t, line cap=round] 
                        let \p1=(U), \p2=(V), \p3=(W), \p4=(Segment) in 
                            plot (\eval{0}{(1/4-\t)}); }{}
            }
        }
        
    \end{tikzpicture}
}

\newcommand{\setFunction}[2]{
    \ifthenelse{#2=0}{\expandafter\def\csname Shape#1\endcsname{\shapeA}}{}
    \ifthenelse{#2=1}{\expandafter\def\csname Shape#1\endcsname{\shapeB}}{}
    \ifthenelse{#2=2}{\expandafter\def\csname Shape#1\endcsname{\shapeC}}{}
    \ifthenelse{#2=3}{\expandafter\def\csname Shape#1\endcsname{\shapeD}}{}}
        
% #1 shape function index i
% #2 shape function index j
% #3 scaling
% #4 color value
% #5 name
% #6 resolution
\newcommand{\drawShapeIJ}[6]
{
    \setFunction{I}{#1}
    \setFunction{J}{#2}
    
    \ifthenelse{#1 < 2 \AND #2 < 2}
        {\drawShape{\ShapeI}{\ShapeJ}{#3}{#4}{#5_#1#2}{2}}
        {\drawShape{\ShapeI}{\ShapeJ}{#3}{#4}{#5_#1#2}{#6}}
}

\definecolor{niceblue}{rgb}{0,0.3,1.0}
\definecolor{darkgreen}{rgb}{0,0.5,0} 
\definecolor{niceviolet}{rgb}{0.5,0,1} 

% ==================== p-fem and others ======================

\newcommand{\DefineU}[1]{\coordinate (U) at ($(2/3*#1, 0)$)}
\newcommand{\DefineV}[1]{\coordinate (V) at ($(2/15*#1, 0.4*#1)$)}
\newcommand{\DefineW}[1]{\coordinate (W) at ($-0.21*(U) + 0.69*(V)$)}
\newcommand{\DefineBoundingBox}[1]{\useasboundingbox ($(O) - 0.5*(V)$) rectangle ($(O) + (U) + 2*(V)$)}

\foreach \ColorValue/\ColorName in {black!14/gray, 
                                    red!55/red, 
                                    niceblue!45/blue, 
                                    darkgreen!45/green, 
                                    niceviolet!40/violet}
{
    \foreach \Scaling/\Resolution/\Name in {0.065/4/pfem_small, 
                                            0.13/8/pfem_medium, 
                                            0.26/12/pfem_large}
    {
        \foreach \i in {0,...,3}
            \foreach \j in {0,...,3}
                {\drawShapeIJ{\i}{\j}{\Scaling}{\ColorValue}{\Name _\ColorName}{\Resolution}}
    }
    
    \foreach \ShapeR [count=\iShape from 0] in {\shapeA, \shapeB}
        {\drawShape{\ShapeR}{\shapeConstant}{0.13}{\ColorValue}{petrov_\ColorName _\iShape0}{2}}
}

\foreach \i in {0,...,3}
    \foreach \j in {0,...,3}
        {\drawShapeIJ{\i}{\j}{0.209}{black!14}{tensorproduct_gray}{12}}

% ============= Multi-level hp sketch ===============

\renewcommand{\DefineU}[1]{\coordinate (U) at ($(#1, 0)$)}
\renewcommand{\DefineV}[1]{\coordinate (V) at ($(0.2*#1, 0.75*#1)$)}

\foreach \ColorValue/\ColorName in {red!55/red, 
                                    darkgreen!45/green}
{
    \foreach \Scaling/\Name/\Resolution/\P in {0.20/sketchA/15/3, 
                                               0.10/sketchB/12/3, 
                                               0.05/sketchC/6/2}
    {
        \foreach \i in {0,...,\P}
            \foreach \j in {0,...,\P}
                {\drawShapeIJ{\i}{\j}{\Scaling}{\ColorValue}{\Name _\ColorName}{\Resolution}}
    }
}

% ============= Multi-level hp shape functions ===============

% Level:     Grid cell sizes:               max. polynomial degree:
% 0          4.0 * 0.056384739 * 478pt      4
% 1          2.0 * 0.056384739 * 478pt      3
% 2          1.3 * 0.056384739 * 478pt      2
%
% Scaling of basis-vectors with 15% gap:
% Level 0: 4.0 * 0.056384739 / (4 + 5 * 0.15) = 0.047481885
% Level 1: 2.0 * 0.056384739 / (3 + 4 * 0.15) = 0.031324855
% Level 2: 1.3 * 0.056384739 / (2 + 3 * 0.15) = 0.029918433

\def\scalingA{0.047481885}
\def\scalingB{0.031324855}
\def\scalingC{0.029918433}

\foreach \ColorValue/\ColorName in {black!14/gray, 
                                    red!55/red, 
                                    niceblue!45/blue, 
                                    darkgreen!45/green, 
                                    niceviolet!40/violet}
{
    \foreach \Scaling/\Name/\Resolution/\P in {\scalingA/shapesA/12/3, 
                                               \scalingB/shapesB/6/2, 
                                               \scalingC/shapesC/2/1}
    {
        \foreach \i in {0,...,\P}
            \foreach \j in {0,...,\P}
                {\drawShapeIJ{\i}{\j}{\Scaling}{\ColorValue}{\Name _\ColorName}{\Resolution}}
    }
}  

% ============= Multi-level hp basis functions ===============

% \def\gap{1}
% \def\skew{0.2} 
% \def\unit{478pt*0.997/(4*(8 + \gap) - \gap + 8*\skew + \skew)}
\def\scalingC{0.0270923913043} % Level 2: 0.997/(4*(8 + \gap) - \gap + 8*\skew)
\def\scalingB{0.0541847826087} % Level 1: 2*\scalingC
\def\scalingA{0.1083695652174} % Level 0: 4*\scalingC

\foreach \ColorValue/\ColorName in {black!14/gray, 
                                    darkgreen!45/green}
{
    \foreach \Scaling/\Factor/\Name/\Resolution/\P in {\scalingA/0.75/basisA/12/3, 
                                                       \scalingB/1.50/basisB/6/2, 
                                                       \scalingC/3.00/basisC/2/1}
    {
        \renewcommand{\DefineW}[1]{\coordinate (W) at ($\Factor*($-0.21*(U) + 0.69*(V)$)$)}
        \renewcommand{\DefineBoundingBox}[1]{\useasboundingbox ($(O) - (V)$) rectangle ($(O) + (U) + {1 + \Factor * 0.75}*(V)$)}
    
        \foreach \i in {0,...,\P}
            \foreach \j in {0,...,\P}
                {\drawShapeIJ{\i}{\j}{\Scaling}{\ColorValue}{\Name _\ColorName}{\Resolution}}
    }
}  

\end{document}
