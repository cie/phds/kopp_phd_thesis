\begin{tikzpicture}
                  
  % Styles        
  \tikzset{faceStyle/.style={fill=black!15, draw=none}}
  \tikzset{edgeStyle/.style={semithick}}
  \tikzset{nodeStyle/.style={draw=black, thick}}

  \def\nodeThickness{0.025}

  % The base coordinates for each drawing
  \coordinate (base) at (1, 1.9);
  \coordinate (classic) at (0, 0);
  \coordinate (mlhpBase) at (3, 0);
  \coordinate (mlhpOverlay) at (3, 1);
  
  % The direction vectors
  \coordinate (U) at (1,0);
  \coordinate (V) at (0.2,0.75);
          
  \coordinate (u) at ($0.5*(U)$);
  \coordinate (v) at ($0.5*(V)$);
  
  % The scaling of U and V for offsetting topological components
  \def\diffU{0.06};
  \def\diffV{0.06};
  
  % --------------------------- Define nodes -----------------------------
  
  % Unrefined                           %            4 ----------- 5 ----------- 6 
  \coordinate (B1) at (base);           %           /             /             /
  \coordinate (B2) at ($(B1) + (U)$);   %          /             /             /
  \coordinate (B3) at ($(B2) + (U)$);   %         /             /             / 
  \coordinate (B4) at ($(B1) + (V)$);   %        /             /             /
  \coordinate (B5) at ($(B2) + (V)$);   %       /             /             /
  \coordinate (B6) at ($(B3) + (V)$);   %      1 ----------- 2 ----------- 3

  % Classical hp
  \coordinate (C1)  at (classic);     
  \coordinate (C2)  at ($(C1) + (U)$);  %            4 ----------- 5 ---- 11 --- 6 
  \coordinate (C3)  at ($(C2) + (U)$);  %           /             /      /      /
  \coordinate (C4)  at ($(C1) + (V)$);  %          /             /      /      /
  \coordinate (C5)  at ($(C2) + (V)$);  %         /             8 ---- 9 ---- 10
  \coordinate (C6)  at ($(C3) + (V)$);  %        /             /      /      /
  \coordinate (C7)  at ($(C2) + (u)$);  %       /             /      /      /
  \coordinate (C8)  at ($(C2) + (v)$);  %      1 ----------- 2 ---- 7 ---- 3
  \coordinate (C9)  at ($(C8) + (u)$);  
  \coordinate (C10) at ($(C9) + (u)$); 
  \coordinate (C11) at ($(C5) + (u)$);
  
  \coordinate (C2A) at ($(C2) - 0.03*(U)$);
  \coordinate (C2B) at ($(C2) + 0.025*(U)$);
  \coordinate (C8B) at ($(C8) + 0.025*(U)$);
  
  % mlhp base mesh                      %            4 ----------- 5 ----------- 6 
  \coordinate (MB1) at (mlhpBase);      %           /             /             /
  \coordinate (MB2) at ($(MB1) + (U)$); %          /             /             /
  \coordinate (MB3) at ($(MB2) + (U)$); %         /             /             / 
  \coordinate (MB4) at ($(MB1) + (V)$); %        /             /             /
  \coordinate (MB5) at ($(MB2) + (V)$); %       /             /             /
  \coordinate (MB6) at ($(MB3) + (V)$); %      1 ----------- 2 ----------- 3

  % mlhp overlay
  \coordinate (MO1) at ($(mlhpOverlay) + (U)$); 
  \coordinate (MO2) at ($(MO1) + (u)$); %
  \coordinate (MO3) at ($(MO1) + (U)$); %                          7 ---- 8 ---- 9 
  \coordinate (MO4) at ($(MO1) + (v)$); %                         /      /      /
  \coordinate (MO5) at ($(MO2) + (v)$); %                        /      /      /
  \coordinate (MO6) at ($(MO3) + (v)$); %                       4 ---- 5 ---- 6
  \coordinate (MO7) at ($(MO1) + (V)$); %                      /      /      /
  \coordinate (MO8) at ($(MO2) + (V)$); %                     /      /      /
  \coordinate (MO9) at ($(MO3) + (V)$); %                    1 ---- 2 ---- 3

  % --------------------------- Draw components -----------------------
  
  % Base mesh faces
  \foreach \P in {B1, B2, C1, MB1, MB2}
  {
    \draw[faceStyle] ($(\P)             + \diffU*(U) + \diffV*(V)$) -- 
                     ($(\P) + (U)       - \diffU*(U) + \diffV*(V)$) -- 
                     ($(\P) + (U) + (V) - \diffU*(U) - \diffV*(V)$) -- 
                     ($(\P) + (V)       + \diffU*(U) - \diffV*(V)$); 
  }
         
  % Base mesh edges in x
  \foreach \P in {B1, B2, B4, B5, C1, C4, MB1, MB2, MB4, MB5}
  {
    \draw[edgeStyle] ($(\P) + \diffU*(U)$) -- ($(\P) + (U) - \diffU*(U)$);
  }
  
  % Base mesh edges in y
  \foreach \P in {B1, B2, B3, C1, C2A, MB1, MB2, MB3}
  {
    \draw[edgeStyle] ($(\P) + \diffV*(V)$) -- ($(\P) + (V) - \diffV*(V)$);
  }
  
  % Base mesh edges in y - red
  \foreach \P in {C2A}
  {
    \draw[edgeStyle, red] ($(\P) + \diffV*(V)$) -- ($(\P) + (V) - \diffV*(V)$);
  }
  
  % Draw dotted lines that connect base and overlay mesh
  \coordinate (difference) at ($(mlhpOverlay) - (mlhpBase)$);
  
  \foreach \C in {MB2, MB3, MB5, MB6}
  {
    \draw[dotted] ($(\C) + \diffU*(difference)$) -- ($(\C) + (difference) - \diffU*(difference)$);
  }
                  
  % Refined edges in x
  \foreach \P in {C2, C7, C8, C9, C5, C11, MO1, MO2, MO4, MO5, MO7, MO8}
  {
    \draw[edgeStyle] ($(\P) + \diffU*(U)$) -- ($(\P) + (u) - \diffU*(U)$);
  }
  
  % Refined edges in y
  \foreach \P in {C7, C3, C9, C10, MO2, MO3, MO5, MO6}
  {
    \draw[edgeStyle] ($(\P) + \diffV*(V)$) -- ($(\P) + (v) - \diffV*(V)$);
  }
  
  % Refined edges in y - red
  \foreach \P in {C2B, C8B, MO1, MO4}
  {
    \draw[edgeStyle, red] ($(\P) + \diffV*(V)$) -- ($(\P) + (v) - \diffV*(V)$);
  }
             
  % Refined faces
  \foreach \P in {C2, C7, C8, C9, MO1, MO2, MO4, MO5}
  {
    \draw[faceStyle] ($(\P)             + \diffU*(U) + \diffV*(V)$) -- 
                     ($(\P) + (u)       - \diffU*(U) + \diffV*(V)$) -- 
                     ($(\P) + (u) + (v) - \diffU*(U) - \diffV*(V)$) -- 
                     ($(\P) + (v)       + \diffU*(U) - \diffV*(V)$); 
  }
        
  % Nodes
  \foreach \C in {B1, B2, B3, B4, B5, B6, 
                  C1, C2, C3, C4, C5, C6, C7, C9, C10, C11,
                  MB1, MB2, MB3, MB4, MB5, MB6, 
                  MO2, MO3, MO5, MO6, MO8, MO9}
  {
    \draw[nodeStyle] (\C) circle (\nodeThickness);
  }
  
  % Nodes - red
  \foreach \C in {C8B, MO1, MO4, MO7}
  {
    \draw[nodeStyle, red] (\C) circle (\nodeThickness);
  }
  
  % --------------------------- Annotations ---------------------------
  
  \node at ($(B2) - (0, 0.15)$) {Before refinement};
  \node at ($(C2) - (0, 0.15)$) {Classical $hp$};
  \node at ($(MB2) - (0, 0.15)$) {Multi-level $hp$};
  
  \draw[->, thick] ($(base) + (U) - (0.2, 0.3)$) -- ($(C5) + (0.1, 0.2)$);
  \draw[->, thick] ($(base) + (U) + (0.3, -0.3)$) -- ($(MB4) + (0.1, 0.2)$);

\end{tikzpicture}
