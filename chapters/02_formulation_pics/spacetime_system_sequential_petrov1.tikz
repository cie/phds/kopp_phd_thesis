\begin{tikzpicture}[every left delimiter/.style={xshift=.5em},
                    every right delimiter/.style={xshift=-.5em}]

\def\colorA{red}
\def\colorB{violet}
\def\colorC{blue}

\tikzset{vectorstyle/.style=
{
    matrix of nodes,
    left delimiter=(,
    right delimiter=),
    inner xsep=0.1cm,
    nodes=
    {
        inner xsep=0.1cm, 
        inner ysep=0cm,
        minimum height=0.65cm, 
        anchor=north east,
        %draw=black
    }
}}

\tikzset{matrixright/.style={vectorstyle, nodes={text width=0.75cm}}}
\tikzset{matrixleft/.style={vectorstyle, nodes={text width=0.55cm}}}

\newcommand{\zero}{\hfill$0$}
\newcommand{\CA}[1]{\textcolor{\colorA}{\hfill$#1$}}
\newcommand{\CB}[1]{\textcolor{\colorB}{\hfill$#1$}}
\newcommand{\CC}[1]{\textcolor{\colorC}{\hfill$#1$}}

% ----------------- First row -----------------
\matrix (K11) [matrixleft]
{ 
    \CA{ 22} & \CA{ -8} & \CA{  0} \\
    \CA{ -8} & \CA{ 22} & \CA{ -8} \\  
    \CA{  0} & \CA{ -8} & \CA{ 11} \\
};

\matrix (U1) [vectorstyle, right=0.55 of K11] 
{ 
    $\hat{u}_0$ \\
    $\hat{u}_1$ \\  
    $\hat{u}_2$ \\
};

\matrix (F1) [vectorstyle, right=of U1] 
{ 
    \CA{2} \\
    \CA{2} \\  
    \CA{1} \\
};

\node at ($(K11.east)!0.55!(U1.west)$) {$\cdot$};
\node at ($(U1.east)!0.5!(F1.west)$) {$=$};

% ----------------- Second row -----------------
\matrix (K22) [matrixleft, below=0.33 of K11]
{ 
    \CB{ 22} & \CB{ -8} & \CB{  0} \\
    \CB{ -8} & \CB{ 22} & \CB{ -8} \\
    \CB{  0} & \CB{ -8} & \CB{ 11} \\  
};

\matrix (U2) [vectorstyle, right=0.55 of K22] 
{ 
    $\hat{u}_3$ \\
    $\hat{u}_4$ \\
    $\hat{u}_5$ \\  
};

\matrix (F2) [vectorstyle, right=of U2] 
{ 
    \CA{2} \\
    \CA{2} \\
    \CA{1} \\
};

\matrix (K21) [matrixright, right=0.8 of F2]
{ 
    \CB{ 14} & \CB{-10} & \CB{  0} \\
    \CB{-10} & \CB{ 14} & \CB{-10} \\
    \CB{  0} & \CB{-10} & \CB{  7} \\  
};

\matrix (U1) [vectorstyle, right=0.55 of K21] 
{ 
    $0.19$  \\
    $0.26$  \\  
    $0.28$  \\
};

\node at ($(K22.east)!0.5!(U2.west)$) {$\cdot$};
\node at ($(U2.east)!0.5!(F2.west)$) {$=$};
\node at ($(F2.east)!0.5!(K21.west)$) {$-$};
\node at ($(K21.east)!0.5!(U1.west)$) {$\cdot$};

% ----------------- Third row -----------------
\matrix (K33) [matrixleft, below=0.33 of K22]
{ 
    \CC{22} & \CC{-8} & \CC{ 0} \\
    \CC{-8} & \CC{22} & \CC{-8} \\
    \CC{ 0} & \CC{-8} & \CC{11} \\
};


\matrix (U3) [vectorstyle, right=0.55 of K33] 
{ 
    $\hat{u}_6$ \\
    $\hat{u}_7$ \\
    $\hat{u}_8$ \\
};

\matrix (F3) [vectorstyle, right=of U3] 
{  
    \CA{2} \\
    \CA{2} \\
    \CA{1} \\
};

\matrix (K32) [matrixright, right=0.8 of F3]
{ 
    \CC{ 14} & \CC{-10} & \CC{  0} \\
    \CC{-10} & \CC{ 14} & \CC{-10} \\
    \CC{  0} & \CC{-10} & \CC{  7} \\
};

\matrix (U2) [vectorstyle, right=0.55 of K32] 
{ 
    $0.23$ \\
    $0.37$ \\
    $0.42$ \\  
};

\node at ($(K33.east)!0.5!(U3.west)$) {$\cdot$};
\node at ($(U3.east)!0.5!(F3.west)$) {$=$};
\node at ($(F3.east)!0.5!(K32.west)$) {$-$};
\node at ($(K32.east)!0.5!(U2.west)$) {$\cdot$};

% ------------- Solution vectors --------------

% Third row
\matrix (U3) [vectorstyle, right=3.2 of K32] 
{ 
    $\hat{u}_6$ \\
    $\hat{u}_7$ \\
    $\hat{u}_8$ \\  
};

\matrix (U3V) [vectorstyle, right=0.8 of U3] 
{ 
    $0.27$ \\
    $0.41$ \\
    $0.46$ \\  
};

\node at ($(U3.east)!0.5!(U3V.west)$) {$\approx$};
\node[left] at (U3.west) {$\to$};

% Second row
\matrix (U2) [vectorstyle] at (K22-|U3) 
{ 
    $\hat{u}_3$ \\
    $\hat{u}_4$ \\
    $\hat{u}_5$ \\  
};

\matrix (U2V) [vectorstyle, right=0.8 of U2] 
{ 
    $0.23$ \\
    $0.37$ \\
    $0.42$ \\
};

\node at ($(U2.east)!0.5!(U2V.west)$) {$\approx$};
\node[left] at (U2.west) {$\to$};

% First row
\matrix (U1) [vectorstyle] at (K11-|U3) 
{ 
    $\hat{u}_0$ \\
    $\hat{u}_1$ \\
    $\hat{u}_2$ \\  
};

\matrix (U1V) [vectorstyle, right=0.8 of U1] 
{ 
    $0.19$ \\
    $0.26$ \\
    $0.28$ \\  
};

\node at ($(U1.east)!0.5!(U1V.west)$) {$\approx$};
\node[left] at (U1.west) {$\to$};

\end{tikzpicture}