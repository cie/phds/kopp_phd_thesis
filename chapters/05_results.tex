
\CopiedChapter{\footnotemark[1]\textsuperscript{,}\footnotemark[2]}{Results}
\label{sec:results}
{

\renewcommand\picsDir{chapters/05_results_pics}


\CopiedSection{\footnotemark[1]}{Singular benchmark}
\label{sec:results:singular}
{

\definecolor{darkgreen}{rgb}{0,0.4,0} 
\definecolor{darkbrown}{rgb}{0.5, 0.396, 0.09}
%
\pgfplotscreateplotcyclelist{convergenceColorList}
{%
  {red, mark=o},  
  {blue, mark=square},
  {cyan, mark=x},
  {darkgreen, mark=diamond},
  {darkbrown, mark=pentagon},
  {black, mark=triangle},
}

\newcommand{\logSlopeTriangleLower}[5]
{
    \pgfplotsextra
    {
        \pgfmathsetmacro{\xA}{#1}
        \pgfmathsetmacro{\xB}{#3}
        \pgfmathsetmacro{\slope}{#4}
        
        % y2 = y1 * 10^( log10(x2/x1) * slope )
        \coordinate (A) at (axis cs:\xA,#2);
        \coordinate (B) at (axis cs:\xB,{#2 * 10^(log10(\xB/\xA)*\slope)});
        
        \draw[#5] (A) -- (B) -- node[right] {\slope} (A-|B) -- node[below] {1} cycle;  
    }
}

\newcommand{\logSlopeTriangleUpper}[5]
{
    \pgfplotsextra
    {
        \pgfmathsetmacro{\xA}{#1}
        \pgfmathsetmacro{\xB}{#3}
        \pgfmathsetmacro{\slope}{#4}
        
        % y2 = y1 * 10^( log10(x2/x1) * slope )
        \coordinate (A) at (axis cs:\xA,#2);
        \coordinate (B) at (axis cs:\xB,{#2 * 10^(log10(\xB/\xA)*\slope)});
        
        \draw[#5] (A) -- (B) -- node[above] {1} (A|-B) -- node[left] {\slope} cycle;  
    }
}

\begin{figure}[h]
    \begin{minipage}[b]{0.425\textwidth}
        \centering
        \includegraphics[width=\textwidth]{\picsDir/04_singular_lshaped_domain.png}
        \subcaption{L-shaped domain}
        \label{fig:results:singular:lshaped}
    \end{minipage}
    %\hspace{0.01\textwidth}
    \begin{minipage}[b]{0.425\textwidth}
        \centering
        \includegraphics[width=\textwidth]{\picsDir/04_singular_fichera_corner.png}
        \subcaption{Fichera corner}
        \label{fig:results:singular:fichera}
    \end{minipage}    
	\hfill
	\begin{minipage}[b]{0.13\textwidth}
        \hfill
		\vspace{1cm}
		\raisebox{0.5cm}{\includeTikz{}{04_singular_colorbar_solution_vertical}}
    \end{minipage}
	\caption{Singular benchmark solutions. \reprintedA}
\end{figure}

\begin{figure}[h]
    \begin{minipage}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{\picsDir/04_singular_3D_solution.png}
        
        \vspace{0.2cm}
        \includeTikz{}{04_singular_colorbar_solution}%
        \subcaption{Finite element solution}
        \label{fig:results:singular:solution}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.48\textwidth}
        \centering
        \includegraphics[width=0.46\textwidth]{\picsDir/04_singular_3D_level1.png}
        \hfill
        \includegraphics[width=0.46\textwidth]{\picsDir/04_singular_3D_level2.png}
        
        \vspace{0.2cm}
        \includegraphics[width=0.46\textwidth]{\picsDir/04_singular_3D_level3.png}
        \hfill
        \includegraphics[width=0.46\textwidth]{\picsDir/04_singular_3D_level4.png}
        
        \vspace{0.2cm}
        \includeTikz{}{04_singular_colorbar_levels}%
        \subcaption{Graded polynomial degrees for $r = 1$ to $r = 4$}
        \label{fig:results:singular:degreedistribution}
    \end{minipage}
        \caption{Singular benchmark on one octant with four refinement levels. \reprintedA}
\end{figure}

In this first example, the presented multi-level $hp$-algorithms and their implementation are verified using a linear Poisson problem 
\begin{alignat}{3}
\nabla \cdot ( \kappa \nabla u ) &= f \qquad &&\text{on } \Omega \nonumber \\
u &= g && \text{on } \Gamma_D      \label{eq:results:singular:strongform}  \\
\kappa \nabla u \cdot n &= h && \text{on } \Gamma_N, \nonumber
\end{alignat}
%
where $\Gamma_D \cup \Gamma_N = \partial \Omega$ and $\Gamma_D \cap \Gamma_N = \emptyset$. The weak form of~\eqref{eq:results:singular:strongform} reads as follows: Find $u \in u_g + H^1_0( \Omega )$, such that
%
\begin{equation*}
\int_{\Omega} \nabla w \cdot \kappa \nabla u \, \text{d}\Omega = \int_{\Omega} w f \; \text{d}\Omega + \int_{\Gamma_N} w h \; \text{d}\Gamma_N \qquad \forall w \in H^1_0(\Omega).
\end{equation*}
%
The manufactured solution is defined as follows:
%
\begin{equation}
  \label{eq:results:singular:solution}
  u = \sqrt{r}  \qquad \text{with} \qquad
  r = \sqrt{\sum_{i=0}^{d-1} x_i^2}
\end{equation}
%
on $\Omega = [0, 1]^d$ with $d \geq 2$, see Figure~\ref{fig:results:singular:solution}. Substituting~\eqref{eq:results:singular:solution} into~\eqref{eq:results:singular:strongform} and using  $\kappa = 1$ yields
%
\begin{equation*}
  f = \frac{3 - 2d}{4} r^{-3/2}.
\end{equation*}
%
Homogeneous Neumann boundary conditions are imposed on the boundaries intersecting the coordinate planes: the left and bottom edges in two dimensions and the left, bottom, and front faces in three dimensions. Dirichlet boundary conditions are imposed on the remaining faces according to \eqref{eq:results:singular:solution}. The finite element discretization of \eqref{eq:results:singular:strongform} results in a linear equation system $\mathbf{K}\boldsymbol{\hat{u}}=\boldsymbol{F}$.
In two dimensions, this benchmark is equivalent to computing one quadrant of an L-shaped domain and exploiting the symmetry of the solution (Figure~\ref{fig:results:singular:lshaped}). Similarly, the three-dimensional analog is computing one octant of a Fichera cube with a corner singularity (Figure~\ref{fig:results:singular:fichera}). 

\begin{figure}[h!]
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_2D}%
        \subcaption{2D}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_3D}%
        \subcaption{3D}
    \end{minipage}
    \newline
    \vspace{0.5em}
    \newline
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_4D}%
        \subcaption{4D}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_5D}%
        \subcaption{5D}
    \end{minipage}
    \caption{Convergence of the error in the energy norm. \reprintedA}
    \label{fig:results:singular:convergence}
\end{figure}

\begin{figure}[h!]
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_iterations}%
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_nnz}%
    \end{minipage}
    
    \vspace{0.4cm}
    
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_runtime_assembly}%
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.48\textwidth}
        \includeTikz{width=\textwidth, height=0.9\textwidth}{04_singular_runtime_all}%
    \end{minipage}
    \caption{Number of \acrshort{CG} iterations, number of non-zeros in the sparse matrix, and serial runtime comparison for the uniformly graded tensor spaces. The total runtime excludes the error computation. \reprintedA}
    \label{fig:results:singular:performance}
\end{figure}

To obtain exponential convergence, the finite element mesh must be refined locally towards the point singularity while elevating the polynomial degree elsewhere to approximate the smooth solution appropriately. The domain $\Omega = [0, 1]^d$ is discretized using a base mesh with two elements in each direction. Two refinement studies are performed, where each new computation adds another refinement level towards the singularity at the origin. The first strategy uses a full tensor-product space with a uniform polynomial degree $p$ equal to $r + 1$, where $r$ is the maximum refinement level. The second strategy employs a trunk space with a linear grading from $p = r + 1$ on the base elements to $p = 1$ on the elements with refinement level $r$. Figure~\ref{fig:results:singular:degreedistribution} shows the graded polynomial degree distribution with one to four \review{refinement} levels and the numerical solution using a uniform tensor-product space with $p = 5$ and $r = 4$. Figure~\ref{fig:results:singular:convergence} shows the convergence results for different spatial dimensions.


While the convergence of the discretization error versus the number of unknowns is necessary to gain insight into the method's approximation quality, the method's success ultimately depends almost exclusively on runtime performance and memory consumption. The following analysis shows that \review{the method achieves} exponential convergence in terms of these crucial quantities. The finite element linear systems are solved using a diagonally preconditioned \gls*{CG} method. Therefore, the number of non-zeros in the matrix dominates the memory consumption. The two most significant contributions to the runtime are the assembly and the solution of the linear system. 

\def\rSimP{\underset{r \, \sim \, p}{\longrightarrow}}

The asymptotic behavior of these quantities is analyzed in terms of the refinement study index $r$ to allow comparisons with runtime measurements. For brevity, only the full tensor-product space using a uniform polynomial degree elevation is considered. Each new refinement level adds $2^d$ cells that overlay the cell in the origin of the parent level. Therefore, the number of elements (leaf cells) can be computed as $r(2^d - 1) + 1 = \mathcal{O} \big ( r \big )$.

Each leaf cell contributes to at most $(p + 1)^d \sim p^d$ basis functions defined on the same refinement level, but it also receives contributions from all coarser levels. The key to obtaining good scaling is that parent cells only contribute basis functions active on their faces. This results in $(p + 1)^d - (p - 1)^d \sim p^{d - 1}$ functions per (coarser) level at most. Because $r \sim p$, these contributions scale with 
%
\begin{equation*}
  \mathcal{O} \big ( r p^{d - 1} \big ) \rSimP \mathcal{O} \big ( p^d \big )
\end{equation*}
%
and hence are of the same order as the $(p - 1)^d = \mathcal{O} \big ( p^d \big )$ internal functions coming from the classical $p$-version of the finite element method. 

The number of non-zeros in the assembled matrix can be bounded to some value between the sum of all element matrix sizes (upper bound) and the sum of all internal contributions (lower bound). Using $\mathcal{O} \big ( r p^{d - 1} +  p^{d} \big )$ as an upper bound for the number of element unknowns and multiplying with $O(r)$ number of elements yields
%
\begin{equation*}
\mathcal{O} \big ( r \big ) \, \mathcal{O} \big ( r p^{d - 1} +  p^{d} \big )^2 \rSimP \mathcal{O} \big (p^{2d + 1} \big )
\end{equation*}
%
as an estimate of the complexity for the number of non-zeros in the assembled sparse matrix. The same result is obtained when considering only internal contributions.

Similarly, the assembly effort is estimated by summing up the number of operations needed to integrate each element matrix numerically. Assuming a standard Gauss-Legendre quadrature, one needs $(p + 1)^d = \mathcal{O} \big ( p^d \big )$ integration points, on which an outer product of $\mathcal{O} \big ( rp^{d - 1} + p^{d} \big )$ shape functions is evaluated, resulting in the following estimate:
%
\begin{equation*}
  \mathcal{O} \big (r \big ) \, \mathcal{O} \big (p^d \big ) \, \mathcal{O} \big (r p^{d - 1} +  p^{d} \big )^2 \rSimP \mathcal{O} \big ( p^{3d + 1} \big ).
\end{equation*} 

Figure~\ref{fig:results:singular:performance} \review{verifies} that the number of non-zeros in the sparse matrix scales \review{as derived} above, while the number of \gls*{CG} iterations increases roughly with $p^{d - 1}$; the analysis of this empirical observation depends on the condition number of the sparse matrix that exceeds the scope of this work. The iterative solver terminates when $\sqrt{\boldsymbol{R} \cdot D(\boldsymbol{R})} < 10^{-10}$, where $D(\boldsymbol{R}) = \text{diag}(\mathbf{K})^{-1}\boldsymbol{R}$ is the diagonal preconditioner applied to the residual $\boldsymbol{R} = \mathbf{K}\boldsymbol{\hat{u}} - \boldsymbol{F}$. Moreover, the symmetry of the sparse matrix is not utilized, which significantly simplifies the parallel implementation of the sparse matrix-vector product. By combining the results for the number of non-zeros with the number of iterations, the runtime complexity for the iterative solver is estimated as $\mathcal{O}(p^{3d})$. Figure~\ref{fig:results:singular:performance} shows that the performance measurements verify these theoretical estimates. Although the assembly of the linear system scales with the highest order, it only starts significantly contributing to the total runtime towards the end of the curves. Moreover, direct solvers with a linear complexity with respect to the number of unknowns \review{exist} for such types of meshes~\parencite{ Paszynski2012}.

The measurements change significantly when considering the trunk space with the graded polynomial degree distribution. While the overall scaling is the same, the absolute effort per degree of freedom is higher, and the relative runtime of the assembly increases compared to the linear solution. There are two main reasons for these differences. First, although the fine levels were assigned a lower polynomial degree, they still receive contributions from the higher-order base elements. Due to these, even the finest level with $p = 1$ requires $r + 2$ Gauss-Legendre points per direction to integrate the parent contributions accurately. Second, removing internal modes increases the connectivity in the trunk space, resulting in higher runtime and increased memory consumption per unknown. When considering two interacting shape functions, $N_i$ and $N_j$, the effort for assembling this interaction is proportional to the number of elements $N_i$ and $N_j$ overlap. Internal functions overlap only on one element but functions active on the faces overlap at least two elements, making internal functions cheaper to assemble. After the assembly, however, this interaction results in a single entry in the sparse matrix regardless of the number of elements the supports overlap. 

}

\CopiedSection{\footnotemark[2]}{AMB2018-02 benchmark}
\label{sec:results:amb}

\begin{table}[h]
\centering
  \begin{tabular}{ ll }
    \toprule
    \multicolumn{2}{l}{Laser parameters}                        \\ 
    \midrule
    Speed ($v$)            & \SI{0.8}{\meter\per\second}        \\  
    Power ($P$)            & \SI{179.2}{W}                      \\  
    Absorptivity ($\nu$)   & 0.32                               \\
    Spot size ($D4\sigma$) & \SI{170}{\micro\meter}             \\  
    Depth ($\sigma_z$)     & $0.28 \cdot D4\sigma/4$            \\  
    \bottomrule
  \end{tabular}
  \hspace{1.2cm}
  \begin{tabular}{ ll }
    \toprule
    \multicolumn{2}{l}{Phase change parameters}                 \\ 
    \midrule
    Latent heat ($L$)            & \SI{2.8e5}{\joule\per\kilo\gram}  \\  
    Solid temperature ($u_s$)   & \SI{1290}{\degreeCelsius}          \\
    Liquid temperature ($u_l$) & \SI{1350}{\degreeCelsius}           \\ 
    Initial temperature ($u_0$) & \SI{25}{\degreeCelsius}            \\ 
    Regularization ($S$) & 1, 2, 4                                   \\ 
    \bottomrule
  \end{tabular}
  \caption{Model parameters (see Sections~\ref{sec:formulation:laser} and~\ref{sec:formulation:phasechange}). \reprintedB}
  \label{tab:results:amb:modelparameters}
\end{table}

\begin{table}[h]
\centering
  \begin{tabular}{ lllllllll }
    \toprule
               & \multicolumn{4}{l}{Fine discretization} 
               & \multicolumn{4}{l}{Coarse discretization}                                                     \\ 
    \midrule
    Base mesh  & \multicolumn{4}{l}{$64 \times 64 \times 9 \times 32 \text{ slabs}$} 
               & \multicolumn{4}{l}{$12 \times 12 \times 3 \times 8 \text{ slabs}$}                            \\
    \midrule                                                                                                   
    Refinement & $\tau$ [\si{\milli\second}] & $d_\tau$ & $\sigma_{\tau}$ [\si{\micro\meter}] & $z$-factor     
               & $\tau$ [\si{\milli\second}] & $d_\tau$ & $\sigma_{\tau}$ [\si{\micro\meter}] & $z$-factor     \\
               & 0      & 6    & 100  & 0.5  & 0    & 5.4  & 180   & 0.5                                       \\
               & 0.082  & 5.3  & 120  & 0.5  & 1.2  & 3.5  & 240   & 0.5                                       \\
               & 0.47   & 4.5  & 150  & 0.5  & 6    & 2.5  & 400   & 0.8                                       \\
               & 1.2    & 3.5  & 160  & 0.8  & 30   & 1.5  & 900   & 1                                         \\
               & 4      & 2.5  & 200  & 1    & 100  & 1    & 1100  & 1                                         \\  
               & 16     & 0.5  & 240  & 1    &      &      &       &                                           \\
    \midrule
    Polynomial de- & \multicolumn{4}{l}{2 (uniform)} & \multicolumn{4}{l}{\numlist{2;2;4;4;3;3} (for levels 0} \\
    grees in space & \multicolumn{4}{l}{}            & \multicolumn{4}{l}{to 5, respectively)}                 \\
    \bottomrule
  \end{tabular}
  \caption{Fine and coarse discretization parameters. \reprintedB}
  \label{tab:results:amb:discretization}
\end{table}

The AMB2018-02 benchmark~\parencite{AMB} is commonly used to initially assess the performance of new simulation methods for \gls*{LPBF} processes. The setup features various laser configurations in single strokes on a block of IN625 alloy ($\SI{24.08}{\milli\meter}\times\SI{24.82}{\milli\meter}\times\SI{3.18}{\milli\meter}$). The length of each stroke is \SI{14}{\milli\meter} with a duration of \SI{17.5}{\milli\second}, but the melt pool usually reaches a steady state after at most \SI{2}{\milli\meter} of travel distance. Experimental data from~\textcite{Lane2020} shows the melt pool width and depth for each track as measured from the cross-section area within which the microstructure changed. The melt pool length and cooling rates are estimated using high refresh rate thermal imaging. This section focuses on test case~B on the \gls*{AMMT} and the measurements from~\textcite[Table 3]{ Lane2020} for track number~3. Table~\ref{tab:results:amb:modelparameters} summarizes the simulation model parameters. The thermal properties of IN625 are
%
\begin{alignat*}{3}
c_s(u) &= \left ( 405 + \frac{247 \cdot u}{\SI{1000}{\degreeCelsius}} \right ) \left [ \si[per-mode = fraction]{\joule\per\kilo\gram\per\degreeCelsius} \right ] \qquad && u \leq u_s \\[0.5em]
k(u) &= \left ( 9.5 + \frac{15 \cdot u}{\SI{1000}{\degreeCelsius}} \right ) \left [ \si[per-mode = fraction]{\watt\per\meter\per\degreeCelsius} \right ] \qquad && u \leq u_s
\end{alignat*}
%
for temperatures below the melting point and are assumed constant for higher temperatures. All simulations use a constant mass density $\rho = \SI{8440}{\kilo\gram\per\cubic\meter}$.

\begin{figure}
  \centering
  \begin{minipage}[c]{0.84\linewidth} 
    \vspace{0pt}
    \includeTikz{width=\linewidth, height=0.52\linewidth}{melt_pool_line_plot}%
  \end{minipage}%
  \caption{Temperature in laser travel direction for different phase change parameters. \reprintedB}
  \label{fig:results:amb:lineplot} 
\end{figure}

\begin{figure}[hbt]
  \centering
  \begin{minipage}[b]{0.72\linewidth} 
      \includegraphics[width=\linewidth]{\picsDir/amb_timederivative_xy.png}
      \subcaption{Top view}
  \end{minipage}

  \vspace{0.5cm}
  \begin{minipage}[b]{0.72\linewidth} 
      \includegraphics[width=\linewidth]{\picsDir/amb_timederivative_xz.png}
      \subcaption{Side view}
  \end{minipage}
  
  \vspace{0.3cm}
  \includeTikz{}{amb_timederivative_colorbar}%
  \caption{Time derivative of the temperature field using the fine discretization. \reprintedB}
  \label{fig:results:amb:coolingrate} 
\end{figure}

\begin{table}[hbt!]
  \centering    
  \begin{tabular}{ llll }
    \toprule
    Result               & Length [\si{\micro\meter}] & Width [\si{\micro\meter}] & Depth [\si{\micro\meter}] \\ 
    \midrule             
    Measurements         & 359 ($\sigma = 20$) & 132 ($\sigma = 2$) & 36 ($\sigma = 0.9$) \\
    \midrule                                                              
    No latent heat       & 301                 & 138                & 39.4                \\
    $S = 1$              & 396                 & 129                & 34.8                \\  
    $S = 2$              & 381                 & 129                & 34.8                \\  
    $S = 4$              & 356                 & 129                & 34.8                \\  
    $S = 8$              & 328                 & 130                & 34.8                \\  
    $u_l + 200$          & 354                 & 131                & 35.4                \\
    \midrule
    $u_l + 200$ (coarse) & 353                 & 132                & 35.3                \\
    \bottomrule    
  \end{tabular}
  \caption{Melt pool dimensions for different model parameters in comparison to experimental data. \reprintedB}
  \label{tab:results:amb:meltpooldimensions}
\end{table}

In the first numerical experiments, the model is calibrated to reproduce the experimental data as closely as possible. The melt pool dimensions are estimated from the simulation results by extracting a contour surface at $u = u_s$ and measuring its bounding box's length, width, and depth. The first calibration step identifies suitable values for the absorptivity $\nu$ and the penetration depth $\sigma_z$ of the heat source in~\eqref{eq:formulation:laser:intensity2D} and~\eqref{eq:formulation:laser:penetration} such that the width and depth match \review{those of the experiment}. Then, the phase change regularization is adjusted for the melt pool length to match the experiments. This strategy uses the property that increasing the smoothness of the phase change mainly affects the melt pool length while having a minor effect on its width and depth. 
%
By using a well-resolved discretization, as seen in Table~\ref{tab:results:amb:discretization}, the discretization influence is minimized. All computations use linear polynomials in time combined with a trunk space for the spatial discretization. The additional $z$-factor in Table~\ref{tab:results:amb:discretization} multiplies the refinement width $\sigma_{\tau}$ in the $z$-direction to prevent unnecessary refinement in the region below the melt pool.

Table~\ref{tab:results:amb:meltpooldimensions} shows the results for different phase change regularizations, where $\nu = 0.32$ and $\sigma_z = 0.28 \cdot D4\sigma / 4$. The dimensions for $S = 4$ (equivalent to $u_s = \SI{1170}{\degreeCelsius}$ and $u_l = \SI{1470}{\degreeCelsius}$) and with only increasing $u_l$ to \SI{1550}{\degreeCelsius} are very similar. 
However, Figure~\ref{fig:results:amb:lineplot} shows that the regularization of the phase transition below \SI{1290}{\degreeCelsius} significantly changes the cooling rate after the solidification. This influence may interfere with a prediction of the microstructure formation, for which a regularization towards higher temperatures may be preferable.
Figure~\ref{fig:results:amb:coolingrate} shows the time derivative of the temperature field obtained directly from the space-time finite element discretization. The cooling rates of around \SI{2e6}{\degreeCelsius\per\second} deviate significantly from the measured \SI{1.08e+06}{\degreeCelsius\per\second}, given by~\textcite[Table 3]{ Lane2020}. However, in \textcite{ Lane2020}, the authors advise against using the cooling rate measurements due to motion blur and a limited calibration range. Therefore, the validation of cooling rates for this thermal model remains an open task.
%
\begin{figure}[h]
  \hfill
  \adjustbox{valign=m}{\includegraphics[width=0.72\linewidth]{\picsDir/amb_solution.png}}
  \adjustbox{valign=m, raise=-0.5cm}{\includeTikz{}{colorbar_vertical}}
  
  \vspace{0.4cm}  
  
  \hspace{0.07\linewidth}
  \begin{minipage}[b]{0.7\linewidth} 
  \includegraphics[width=0.6901\linewidth]{\picsDir/amb_solution_contour2D_side.png}
  \hspace{0.03\linewidth}
  \includegraphics[width=0.2597\linewidth]{\picsDir/amb_solution_contour2D_front.png}
  \end{minipage}
  \hfill
  
  \hspace{0.25\linewidth}Side view\hspace{0.287\linewidth}Front view
 
  \caption{Solution and contour surface at $u_s = \SI{1290}{\degreeCelsius}$ for coarse discretization with $u_l = \SI{1550}{\degreeCelsius}$. \reprintedB}
  \label{fig:results:amb:meltpool} 
\end{figure}
%
Next, the discretization is coarsened as much as possible while still obtaining good estimates to showcase the benefits of the presented approach. Figure~\ref{fig:results:amb:meltpool} shows the melt pool geometry and the temperature in the vicinity for the coarse discretization; see Table~\ref{tab:results:amb:discretization}. A simulation time of \SI{17.5}{\milli\second} and eight time slabs results in a duration of about \SI{2.2}{\milli\second} for one slab. The melt pool dimensions (last row of Table~\ref{tab:results:amb:meltpooldimensions}) are almost identical to \review{the} ones of the well-resolved discretization. For simplicity, all elements are over-integrated with $p + 2$ quadrature points in space and $p + 1$ quadrature points in time to capture the phase change accurately. The linear systems are solved with Intel's Pardiso sparse direct solver. 

\CopiedSection{\footnotemark[2]}{Hatched square}
\label{sec:results:hatchedsquare}

\begin{figure}[h]
  \centering
  \includeTikz{}{hatched_square_setup}%
  \caption{Laser path (left) and example discretization (center and right) at $t = \SI{1.2168}{\second}$ for a hatched square with \SI{10}{\milli\meter} side length and \SI{100}{\micro\meter} hatch width. \reprintedB}
  \label{fig:results:hatchedsquare:path} 
\end{figure}

\begin{figure}[h!]
  \includegraphics[width=0.48\linewidth]{\picsDir/hatched_square_1_2168_solution.png}  
  \hfill
  \includegraphics[width=0.48\linewidth]{\picsDir/hatched_square_1_2786_solution.png}
            
  \vspace{0.3cm}
   
  \hspace{0.04\linewidth}  
  \adjustbox{valign=m}
  {
    \adjustbox{valign=b}
    {      
        \includeTikz{}{hatched_square_1_2168_melt_pool}%
        \hspace{0.07\linewidth} 
        \includeTikz{}{hatched_square_1_2786_melt_pool}%
    }
  }
  \hfill
  \adjustbox{valign=m}{\includeTikz{}{colorbar_vertical}}
  \caption{Temperature and melt pool geometry for $t = \SI{1.2168}{\second}$ (left) and $t = \SI{1.2786}{\second}$ (right). \reprintedB}
  \label{fig:results:hatchedsquare:solution} 
\end{figure}  

This section uses the setup of the previous section for the AMB2018-02 benchmark to hatch a square area with a $\SI{10}{\milli\meter}$ side length. As Figure~\ref{fig:results:hatchedsquare:path} shows, the laser path first follows the contour line and then fills the interior with a hatch distance of \SI{100}{\micro\meter}, resulting in a path length of about \SI{102}{\centi\meter}. The entire process takes about \SI{1.28}{\second}, which is extended to a total simulation time of \SI{3}{\second}. During this cooldown period, the discretization is automatically coarsened in space and time as a result of the formulation and the way the meshes are constructed, as Section~\ref{sec:mlhp:lpbfrefinement} discusses. Each time slab contains one base element in time with a duration of \SI{2.4}{\milli\second}, which is slightly longer than in the previous example. The number of unknowns per slab initially averages around $50$ thousand and drops to around \num{2400} for slabs in the cooldown period.

Figure~\ref{fig:results:hatchedsquare:solution} shows the solution and the melt pool dimensions for two time slices; on the left side, the laser approaches the top left corner, and on the right side, it has just reached it. Significant heating of the plate \review{appears}, leading to more than a 50\%~predicted increase in melt pool width and depth for the second case. Hence, this spot is identified as a potential source of defects.

The simulation runs on a single Intel Xeon Gold~6230 CPU with 20~cores running at \SI{2.1}{\giga\hertz} in about 7~hours. Compared to the single-threaded execution, the parallel version is about eight times faster, a good result considering that the CPU's turbo boost frequency is \SI{3.9}{\giga\hertz}, which, from experience, results in a maximum speedup of \numrange{11}{12} times. While this speedup is reached in the assembly of the linear systems, the Pardiso sparse direct solver does not scale optimally in the examples. Moreover, the assembly again over-integrates with $p + 2$ points in space and $p + 1$ points in time. This performance penalty can be improved by identifying the elements around the laser source and increasing the number of quadrature points there only.

\section{Performance comparison to time-stepping}
\label{sec:results:comparison}

\begin{figure}[h!]
  \begin{minipage}[t]{0.64\linewidth}
	  \includeTikz{}{comparison_mesh_00}
      \subcaption{Smallest domain with \SI{1.6}{\centi\meter} width}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.32\linewidth}
      \includePng{width=\linewidth}{comparison_mesh_01}
      \subcaption{Extension to \SI{4.8}{\centi\meter} width}
  \end{minipage}
  
  \vspace{0.5cm}
  
  \begin{minipage}[t]{0.32\linewidth}
      \includePng{width=\linewidth}{comparison_mesh_02}
      \subcaption{Extension to \SI{9.6}{\centi\meter} width}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.32\linewidth}
      \includePng{width=\linewidth}{comparison_mesh_06}
      \subcaption{Extension to \SI{25.6}{\centi\meter} width}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.32\linewidth}
      \includePng{width=\linewidth}{comparison_mesh_13}
      \subcaption{Extension to \SI{86.4}{\centi\meter} width}
  \end{minipage}
  \caption{Benchmark with a small laser stroke in the center and an increasing peripheral cost resulting from adding root elements on the left, right, front, back, and bottom sides. Each mesh is refined seven times. The solution shown is evaluated at $t = \SI{8.75}{\milli\second}$. }
  \label{fig:results:comparison:mesh}
\end{figure}

\begin{figure}[h!]
  \centering
  \includeTikz{width=0.55\linewidth, height=0.45\linewidth}{comparison_ndof}
  \caption{Comparison between single space-time slab and $128$ time steps.}
  \label{fig:results:comparison:ndof}
\end{figure}

\begin{figure}[h!]
  \centering
  \includeTikz{}{comparison_runtime}
  
  \vspace{0.5cm}\hspace{1cm}
  \ref{UniversalLegend}
  \caption{Comparison between single space-time slab and $128$ time steps.}
  \label{fig:results:comparison:runtime}
\end{figure}

\begin{figure}[h!]
  \centering
  \includeTikz{width=0.55\linewidth, height=0.45\linewidth}{comparison_ompspeedup}
  \caption{Speedup of parallel vs. serial execution.}
  \label{fig:results:comparison:ompspeedup}
\end{figure}

This section compares the locally refined space-time discretization in four dimensions to a more conventional three-dimensional locally refined $hp$-approach. The computational domain in space is initially chosen as $[\SI{-8}{\milli\meter}, \SI{8}{\milli\meter}]^3$ and is discretized with a base mesh of two elements per direction. The initial domain and discretization are then gradually extended to $[\SI{-43.2}{\centi\meter}, \SI{43.2}{\centi\meter}]^3$ and a base mesh of $108$ elements per direction. Both discretization types are refined seven levels towards the laser spot using the same level function to enable a meaningful comparison. The time interval is in all cases $[\SI{0}{\milli\second}, \SI{8.75}{\milli\second}]$. Figure~\ref{fig:results:comparison:mesh} shows the increasingly large spatial meshes at $t = \SI{8.75}{\milli\second}$. The time-stepping reference uses $128$ Crank-Nicolson steps ($\theta = 1/2$), while the space-time discretization reduces the element durations automatically by refining locally in four dimensions. On the finest elements, the time interval is identical; however, the space-time mesh also coarsens in time, away from the laser. Both discretizations use the trunk space with polynomial degree $3$ in space. The space-time discretization uses a linear interpolation in time.

In Figure~\ref{fig:results:comparison:ndof}, the number of unknowns for the space-time mesh is initially much \review{larger} than for a single time step mesh. There are only slightly fewer degrees of freedom in the time slab mesh than in the meshes of all time steps together. However, as the domain is extended and more elements are added, the peripheral part of the domain starts to dominate the number of unknowns until, eventually, a single time step is as expensive as a single time slab. 

The relative reduction of the residual norm during the nonlinear iterations of each time step reaches the threshold value of \num{e-10} in about ten iterations (further iterations stagnate at \numrange{e-13}{e-11}). The nonlinear iterations of the single time slab systems of the space-time discretization converge to a relative residual norm reduction threshold of \num{e-14} in about seven iterations (stagnating at around \num{e-16}). The linear systems of the space-time discretization are solved using the Intel Pardiso sparse direct solver. From now on, this approach is called ST-Pardiso. The linear systems of the time-stepping discretizations are solved using two methods: first, also using Intel's Pardiso solver (TS-Pardiso) to compare to ST-Pardiso directly, and second, using a diagonally preconditioned \gls*{CG} method combined with the sparse matrix-vector multiplication of the Intel MKL library (TS-CG). All three approaches (ST-Pardiso, TS-Pardiso, TS-CG) are computed in serial and also using shared-memory (Open-MP) parallelism with 20 cores/threads (again on a single Intel Xeon Gold~6230 CPU). The TS-CG approach uses the standard (unsymmetric) compressed sparse row (CSR) format to store sparse matrices. Although the symmetric version of the CSR format that stores only half of the matrix performs better in the serial case, it is not further investigated in this section to simplify the comparisons. Similarly, TS-Pardiso uses the same setup as ST-Pardiso, where an unsymmetric CSR matrix is given to the Pardiso solver without ''mentioning'' the symmetry of the time-stepping formulation. Potentially faster versions based on either symmetric CSR matrices or unsymmetric CSR matrices with a consistent linearization \eqref{eq:formulation:timestepping:linearization} of \eqref{eq:formulation:timestepping:discreteweakresidual} (leading to fewer nonlinear iterations) are excluded from the following comparison. The inconsistent linearization used by TS-Pardiso and TS-CG does not affect the initial iterations, but it slows the convergence afterwards and requires more iterations to reach \review{machine} precision. Hence, choosing a higher threshold value can reduce the difference in the number of nonlinear iterations between ST-Pardiso and TS-Pardiso/CG. Moreover, the space-time finite element integrals are integrated with four quadrature points per spatial direction ($p + 1$) and one quadrature point in time ($p$). In contrast, the previous sections used one additional quadrature point per direction to improve the accuracy of the results. While these choices may slightly skew the results in favor of the space-time discretization, they do not change the general characteristics discussed in the following.

\begin{figure}[hbt!]
  \centering
  \includeTikz{width=0.55\linewidth, height=0.45\linewidth}{comparison_cgiterations}
  \caption{Average number of \gls*{CG} iterations per Newton-Raphson iteration for TS-CG.}
  \label{fig:results:comparison:cgiterations}
\end{figure}

Figure~\ref{fig:results:comparison:runtime} compares the serial and parallel runtime for ST-Pardiso, TS-Pardiso, and TS-CG approaches. The serial runtime of the space-time discretization is initially about twice as long as TS-Pardiso and TS-CG. Although the number of unknowns of the single slab is lower than the sum of all time steps, the solution of a single large equation system is more expensive than solving many smaller systems. Moreover, the overlap of basis functions in four dimensions is higher than in three, further increasing the runtime and memory consumption. When expanding the domain, the \textit{peripheral computational cost} dominates much later for the space-time discretization, leading to a similar runtime (compared to the initial mesh) for much longer than the time-stepping. While this holds for TS-Pardiso and TS-CG, the iterative solver in TS-CG scales better than the direct solver for ST-Pardiso and TS-Pardiso. When extending the domain, the number of \gls*{CG} iterations does not increase much (i.e., less than five percent). While this example uses constant initial data, the same holds for spatially varying initial temperature fields, where the time step lengths are still so small that the peripheral temperature increments are almost zero. As a result, the runtime of TS-CG eventually catches up to ST-Pardiso once the linear systems become very large. The same results can be observed in the OMP-parallel execution but with two differences. First, on the large initial mesh, all three approaches \review{require} roughly the same amount of time. Figure~\ref{fig:results:comparison:ompspeedup} shows that the parallel efficiency of ST-Pardiso is initially much better than TS-CG and, in particular, TS-Pardiso. The second difference to the serial execution is the improved parallel scaling of the TS-CG method for bigger meshes, as the Intel MKL's sparse matrix-vector multiplication scales better than their sparse direct solver. An interesting characteristic of TS-CG is that later Newton-Raphson iterations with an almost converged solution require fewer \gls*{CG} iterations in the solution of the linear system, as Figure~\ref{fig:results:comparison:cgiterations} shows.

These results show that the space-time discretization has the potential to capture the multi-scale nature of the thermal evolution of \gls*{LPBF} processes better than conventional time-stepping methods. The advantage of local refinement in four dimensions grows with an increased peripheral computational cost. While in this example, more elements were added on the boundary of the base mesh, more realistic simulations can cause an increased peripheral cost in other ways. For instance, resolving complex geometries based on a very heterogeneous material history using immersed methods can be computationally expensive.

}